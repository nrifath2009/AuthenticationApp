﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AjaxExampleApp.Models
{
    public class Product
    {
        public string Name { get; set; }
        public string Code { get; set; }
        public decimal UnitPrice { get; set; }
        public int Quantity { get; set; }


    }
}