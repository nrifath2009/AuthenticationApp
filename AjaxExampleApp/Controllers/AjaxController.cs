﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AjaxExampleApp.Models;

namespace AjaxExampleApp.Controllers
{
    public class AjaxController : Controller
    {
        //
        // GET: /Ajax/
        public ActionResult Index()
        {
            return View();
        }

        private List<Product> GetAllProducts()
        {
            return new List<Product>()
            {
                new Product(){Code = "P-001",Name = "Product-001",UnitPrice = 101},
                new Product(){Code = "P-002",Name = "Product-002",UnitPrice = 102},
                new Product(){Code = "P-003",Name = "Product-003",UnitPrice = 103},
                new Product(){Code = "P-004",Name = "Product-004",UnitPrice = 104},
                new Product(){Code = "P-005",Name = "Product-005",UnitPrice = 105},
                new Product(){Code = "P-006",Name = "Product-006",UnitPrice = 106},
                new Product(){Code = "P-007",Name = "Product-007",UnitPrice = 107},
                new Product(){Code = "P-008",Name = "Product-008",UnitPrice = 108},
                new Product(){Code = "P-009",Name = "Product-009",UnitPrice = 109},
            };
        } 
        public PartialViewResult ShowDetails()
        {
            System.Threading.Thread.Sleep(3000);
            string code = Request.Form["txtCode"];
            List<Product> productList = GetAllProducts();
            Product prod = productList.FirstOrDefault(c => c.Code == code);
            return PartialView("_ShowDetails", prod);
        }
	}
}