﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(AjaxExampleApp.Startup))]
namespace AjaxExampleApp
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
